#================================================================
# HEADER
#================================================================
# SYNOPSIS
#    kintai.ps1
#    (no parameters)
# DESCRIPTION
#    出勤・退勤時間及び
#    実働時間(スリープを除く、PCが稼働していた時間)を算出
# OPTIONS
#    (no options)
# EXAMPLES
#    kintai.ps1
#
#================================================================
# IMPLEMENTATION
#    version         kintai.ps1 1.00
#    author          S.Sasaki(sasaki@cns.co.jp)
#    copyright       Copyright 2016 CNS Co.,Ltd. http://www.cns.co.jp/
#    license         GNU General Public License
#
#================================================================
#  HISTORY
#     2016/10/25 : sasaki : Script creation
# 
#================================================================

#################################################################
# 日付をまたいだ場合等の営業日付切替時刻
#################################################################
$dtchgtime = 4

#################################################################
# 対象とするイベントログ
#################################################################
# 起動系イベント
# Microsoft-Windows-Power-Troubleshooter:1 スリープ状態からの復帰
# Microsoft-Windows-Kernel-General:12 OS起動時
# イベント追加するならboot_idsにIDを追加
$boot_ids = @(1,12)

# 終了系イベント
# Microsoft-Windows-Kernel-Power:42 スリープ状態になる
# Microsoft-Windows-Kernel-General:13 OS終了時
# イベント追加するならshut_idsにIDを追加
$shut_ids = @(13,42)


# 前月月初からを対象とする
$before_ym = (Get-Date).AddMonths(-1)
$ym = New-Object System.DateTime $before_ym.Year,$before_ym.Month,1,$dtchgtime,0,0,0


# 主要データを抽出して$hashに格納
$hash = @{}
Get-EventLog System -After $ym | Where-Object { ($_.Source -eq "Microsoft-Windows-Power-Troubleshooter" -and $_.EventID -eq 1) -or ($_.Source -eq "Microsoft-Windows-Kernel-Power" -and $_.EventID -eq 42) -or ($_.Source -like "Microsoft-Windows-Kernel-General" -and $_.eventID -in(12,13) ) } | ForEach-Object {
  $time = $_.TimeGenerated
  $id = $_.EventID
  
  $hash.Add($time, $id)
}

# 時系列順に開始-終了のペア生成
$pairhash = @{}
$hash.GetEnumerator() | Sort-Object Name | Foreach-Object {
  $time = $_.Name
  $key = $time.ToString('yyyy/MM/dd')
  $id = $_.Value
  
  if ( $id -in $boot_ids ) {
    # 起動のログ #
    $pair = @{"Start" = $time; "End" = $null; "WorkTime" = $null}
  } elseif( $id -in $shut_ids ) {
    # 終了のログ #
    $pair["End"] = $time
    $pair["WorkTime"] = $pair["End"] - $pair["Start"]
    $pairhash.Add($pair["Start"], $pair)
  }
}

# 営業日付を考慮した稼働時間を算出
$result = @{}
$pairhash.GetEnumerator() | Sort-Object Name | Foreach-Object {
  $dt = $_.Name
  # 営業時間切替日までは前日日付と見なすので、キーとなる日付の計算は$dtchgtimeを引く
  $key = ($dt.AddHours(-1 * $dtchgtime)).toString("yyyy/MM/dd")
  
  if ( ! $result.ContainsKey($key) ){
    # キーが存在しない場合は営業日を新規登録
    $result.Add($key, $_.Value)
  } else {
    # キーが登録済みの場合は終了時刻と稼働時間を更新
    $tmphash = $result[$key]
    $end   = $_.Value["End"]
    $wk    = $_.Value["WorkTime"]
    
    $tmphash["End"] = $end
    $tmphash["WorkTime"] = $tmphash["WorkTime"] + $wk
  }
}

# 整形して表示
Write-Output 'Date      	Start	End	WorkTime'
$result.GetEnumerator() | Sort-Object Name | Foreach-Object {
  $dt = $_.Name
  $start = $_.Value["Start"].toString('HH:mm')
  $end   = $_.Value["End"].toString('HH:mm')
  $wk    = $_.Value["WorkTime"]
  
  Write-Output "$dt	$start	$end	$wk"
}
