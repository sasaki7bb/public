#!/bin/bash
#================================================================
# HEADER
#================================================================
# SYNOPSIS
#    setup_delegate.sh
#    (no parameters)
# DESCRIPTION
#    Delegate setup script for RHEL6/7.
# OPTIONS
#    (no options)
# EXAMPLES
#    setup_delegate.sh
#
#================================================================
# IMPLEMENTATION
#    version         ${SCRIPT_NAME} (www.uxora.com) 1.00
#    author          S.Sasaki(sasaki@cns.co.jp)
#    copyright       Copyright 2016 CNS Co.,Ltd. http://www.cns.co.jp/
#    license         GNU General Public License
#
#================================================================
#  HISTORY
#     2016/09/23 : sasaki : Script creation
# 
#================================================================

# work directory
SETUP_DIR="/tmp/delegate_setup"

# path of Delegated
DG_ROOT="/usr/local/delegate"
DG_BIN="${DG_ROOT}/bin"
DG_CONF="${DG_ROOT}/conf"
DG_LOG="/var/log/delegated"

# put delegated's symbolic link into BIN_DIR.
BIN_DIR="/usr/local/bin"

# Administrator's mail address.
ADMIN_MAIL="a@a"

# user check
if [ $(whoami) != "root" ]
then
  echo "Run as root."
  exit 99
fi


mkdir -p ${SETUP_DIR}
cd ${SETUP_DIR}

# wget check
if [ ! $(which wget) ]
then
  # wget isn't installed.
  yum -y install wget
fi

# rpm build check
if [ ! $(which rpmbuild) ]
then
  # rpmbuild isn't installed.
  yum -y install rpm-build
fi

# gcc check
if [ ! $(which gcc) ]
then
  # gcc isn't installed.
  yum -y install gcc
fi

# gc-c++ check
if [ $(rpm -q gcc-c++ > /dev/null 2>&1; echo $?) -ne 0 ]
then
  # gcc-c++ isn't installed.
  yum -y install gcc-c++
fi

# libstdc++ check
if [ $(rpm -q libstdc++ > /dev/null 2>&1; echo $?) -ne 0 ]
then
  # libstdc++ isn't installed.
  yum -y install libstdc++
fi

# libstdc++-devel check
if [ $(rpm -q libstdc++-devel > /dev/null 2>&1; echo $?) -ne 0 ]
then
  # libstdc++-devel isn't installed.
  yum -y install libstdc++-devel
fi

DGSRC=$(curl ftp://delegate.hpcc.jp/pub/DeleGate/ 2>/dev/null | awk '/delegate[0-9\.-]+.tar.gz/{print $NF}')
wget "ftp://delegate.hpcc.jp/pub/DeleGate/${DGSRC}"

tar zxf ${DGSRC}
cd ${DGSRC/.tar.gz/}

# make
make<<EOF
${ADMIN_MAIL}
y
EOF


mkdir -p ${DG_BIN}
mkdir -p ${DG_CONF}

cp -p src/delegated ${DG_BIN}
ln -s ${DG_BIN}/delegated ${BIN_DIR}/delegated

# setup log output directory
mkdir -p ${DG_LOG}
chown -R nobody:nobody ${DG_LOG}

# service registration
wget https://bitbucket.org/sasaki_cns/public/raw/f3c6ae6b5cb8668627fb767ce082de8d30991d00/delegate/delegated -P /etc/rc.d/init.d
chmod +x /etc/rc.d/init.d/delegated
chkconfig --add delegated

# download a sample file
wget https://bitbucket.org/sasaki_cns/public/raw/f3c6ae6b5cb8668627fb767ce082de8d30991d00/delegate/rdp.conf -P ${DG_CONF}

echo "setup finish."
echo "put your config file into ${DG_CONF}, and run delegated."
echo "RHEL6: service delegated start"
echo "RHEL7: systemctl start delegated"
